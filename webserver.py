from flask import Flask, request, send_from_directory, send_file
from multiprocessing import Process,Queue,Pipe
import light_control as light

func = "blink"
delay = 500
refreshRate = 20

def _setLight():
    while True:
        global func, delay, refreshRate
        if func == "blink":
            light.blink( delay )
        elif func == "glow":
            light.glow( delay, refreshRate )

def _getVars( requestForm ):
    for data in requestForm:
        print( str(data), '-', requestForm[data] )
    
    return 'hi'




app = Flask(__name__)

@app.route('/')
def index():
    return send_file( './html/index.html' )
@app.route('/<path:path>')
def indexFiles(path):
    return send_from_directory( './html/', path )


@app.route('/post', methods=['POST'])
def appPost():
    error = None
    if request.method == 'POST':
        return _getVars(request.form)
    # the code below is executed if the request method
    # was GET or the credentials were invalid
    return 'error 0x00000000'

    
parent_conn,child_conn = Pipe()
p = Process( target = light.main(), args=(child_conn,))
p.start()