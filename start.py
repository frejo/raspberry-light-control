from gevent.wsgi import WSGIServer
from webserver import app
import configparser

Config = configparser.ConfigParser()
Config.read( "config.ini" )

ip = Config.get('Server', 'Ip')
port = int(Config.get('Server', 'Port'))

print(ip, port)

http_server = WSGIServer(( ip, port ), app)
http_server.serve_forever()