import argparse
import pigpio
import math
import configparser
import time
import os

CURRENT_DIR = os.path.dirname(__file__)
ConfPath = os.path.join(CURRENT_DIR, 'config.ini')
StatePath = os.path.join(CURRENT_DIR, 'state.ini')

def parseArgs():
    parser = argparse.ArgumentParser( description = 'Change the light')
    parser.add_argument( 'function',
        help = 'The function' )
    parser.add_argument( '-p', '--period', 
        dest = 'p',
        type = int, 
        default = 500,
        help = 'Half a period in ms (default: 500)' )
    parser.add_argument( '-r', '--refresh', 
        dest = 'r',
        type = int, 
        default = 20,
        help = 'Refresh rate in ms (default: 20)' )
    return parser.parse_args()

# Read the config file and import variables: 
Config = configparser.ConfigParser()
Config.read( ConfPath )

State = configparser.ConfigParser()

# Set the ip of the Raspberry Pi and the port the pigpio daemon is listening to
ip = Config.get('Lights', 'Ip')
port = Config.get('Lights', 'Port')

pi = pigpio.pi(ip, port)



lights_a = int( Config.get('Lights', 'Lights_A') )
lights_b = int( Config.get('Lights', 'Lights_B') )

pi.set_mode( lights_a, pigpio.OUTPUT )
pi.set_mode( lights_b, pigpio.OUTPUT )
pi.write( lights_a, 0 )
pi.write( lights_b, 0 )


def millis():
    return int(round(time.time() * 1000))

previousMillis = millis()

aVal = True
pi.write( lights_a, aVal )
pi.write( lights_b, not aVal )

def blink( delay ):
    global previousMillis
    global aVal
    if millis() > previousMillis + delay: 
        previousMillis = millis()
        aVal = not aVal
        pi.write( lights_a, aVal )
        pi.write( lights_b, not aVal )

def glow( halfPeriod, updateTime = 10 ):
    startMillis = millis()
    global previousMillis
    global aVal
    if millis() > previousMillis + updateTime: 
        previousMillis = millis()
        duty = 255 * math.sin( ( math.pi / halfPeriod ) * startMillis )
        if duty < 0:
            aVal = False
        else:
            aVal = True
        #print("A:", lights_a, duty * aVal )
        #print("B:", lights_a, duty * (not aVal) )
        pi.set_PWM_dutycycle( lights_a, abs( duty * aVal ))
        pi.set_PWM_dutycycle( lights_b, abs( duty * (not aVal) ))

def glow2( halfPeriod, runTime = 1000 ):
    startMillis = millis()
    global previousMillis
    global aVal
    while millis() < startMillis + runTime:
        f = 255/2 * math.sin( ( math.pi / halfPeriod ) * millis() )
        pi.set_PWM_dutycycle( lights_a, int(255/2 + f))
        pi.set_PWM_dutycycle( lights_b, 0)
        time.sleep(0.0001)
        pi.set_PWM_dutycycle( lights_a, 0)
        pi.set_PWM_dutycycle( lights_b, int(255/2 - f))
        time.sleep(0.0001)
    

def off():
    pi.write( lights_a, False )
    pi.write( lights_b, False )

def readStateFile():
    State.read( StatePath )
    f =      State.get('State', 'Function'   )
    p = int( State.get('State', 'Period'     ) )
    r = int( State.get('State', 'RefreshRate') )
    return f, p, r

def writeStateFile( f, p, r):
    stateFile = open( StatePath , 'w+')
    if not State.has_section('State'):
        print("x01")
        State.add_section('State')
    State.set  ( 'State', 'Function',        f  )
    State.set  ( 'State', 'Period',      str(p) )
    State.set  ( 'State', 'RefreshRate', str(r) )
    State.write( stateFile )
    stateFile.close()

def main( func, period, refresh ):
    if func == "autostart":
        func, period, refresh = readStateFile()
    else:
        writeStateFile(func, period, refresh)
    prevModTime = os.path.getmtime( StatePath )
    while True:
        curModTime = os.path.getmtime( StatePath )
        if (prevModTime != curModTime):
            prevModTime = curModTime
            func, period, refresh = readStateFile()
        if func == "blink":
            blink( period )
        elif func == "glow":
            glow( period, refresh )
        elif func == "glow2":
            glow2( period, refresh )
        elif func == "off":
            off()
        else:
            off()
            print("Unknown function.")

if __name__ == "__main__":
    args = parseArgs()
    main( args.function, args.p, args.r )