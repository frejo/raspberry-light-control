$( document ).ready( function() {
    // Set variables for the control elements
    var lightFunction = "glow",
    
        blink         = $("#buttonBlink"),
        glow          = $("#buttonGlow"),
        delayVal      = $("#sliderDelay"),
        refreshVal    = $("#sliderRefresh");

    postData = function() {
        url = "/post";
        data = {
            function: lightFunction,
            delay: delayVal.val(),
            refreshrate: refreshVal.val(),
        };
        $.post( url , data , function(data, status){
            //alert("Data: " + data + "\nStatus: " + status);
        });
    };

    // Display default values
    delayVal.val( 500 );
    refreshVal.val( 10 );

    // // Change the value display when sliders change
    // rotationDOM.oninput = function() {
    //     rotationDisplay.val( rotation.val() );
    //     postData()
    // }
    // speedDOM.oninput = function() {
    //     speedDisplay.val( speed.val() );
    //     postData()
    // }

    // Send post with settings on changes
    blink.click( function() {
        lightFunction = "blink";
        postData()
    });
    glow.click( function() {
        lightFunction = "glow";
        postData()
    });
});