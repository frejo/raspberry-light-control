A project to control a light strip using a Raspberry Pi.

# Requirements
python3, pigpio, keyboard (pip), configparser (pip), (flask, gevent)

## Install
```
sudo apt-get update
sudo apt-get install pigpio python-pigpio python
pip install flask 
pip install gevent 
pip install configparser
```
